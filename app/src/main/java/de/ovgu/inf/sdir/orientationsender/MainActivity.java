package de.ovgu.inf.sdir.orientationsender;

import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

import de.ovgu.inf.sdir.orientationsender.sensors.AcceleroGyroSensor;
import de.ovgu.inf.sdir.orientationsender.sensors.INewSensorValueListener;
import de.ovgu.inf.sdir.orientationsender.sensors.ISensor;
import de.ovgu.inf.sdir.orientationsender.sensors.AcceleroMagnetoSensor;
import de.ovgu.inf.sdir.orientationsender.sensors.MagnetoSensor;

public class MainActivity extends AppCompatActivity implements INewSensorValueListener {
    public static final boolean RUN_LATENCY_EVALUATION = false;
    protected static final String TAG = MainActivity.class.getName();
    protected static final String[] SAMPLE_INTERVAL_OPTIONS = {"Fastest", "Game", "UI", "Normal", "Custom (enter below)"};

    protected TextView textView_sensorAzimuth = null;
    protected TextView textView_sensorPitch = null;
    protected TextView textView_sensorRoll = null;
    protected TextView textView_avgSamplingInterval = null;
    protected TextView textView_numSamples = null;
    protected Button button_toggleSending = null;
    protected EditText editText_remoteIpAddress = null;
    protected EditText editText_remotePort = null;
    protected EditText editText_customSamplingInterval = null;
    protected Spinner spinner_samplingInterval = null;

    protected AcceleroGyroSensor acceleroGyroSensor;
    protected AcceleroMagnetoSensor acceleroMagnetoSensor;
    protected MagnetoSensor magnetoSensor;
    protected ISensor currentSensor = null;

    protected int samplingInterval = SensorManager.SENSOR_DELAY_NORMAL;
    protected DatagramSender sender = null;
    protected InetAddress inetAddress = null;
    protected short port = -1;
    protected boolean sendingEnabled = false;

    // statistics
    float[] lastOrientation = {0, 0, 0};
    long numSamples = 0;
    long samplingIntervalSum = 0;
    long lastSampleTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // setup sensors
        acceleroGyroSensor = new AcceleroGyroSensor(getApplicationContext());
        acceleroGyroSensor.registerNewSensorValueListener(this);
        acceleroMagnetoSensor = new AcceleroMagnetoSensor(getApplicationContext());
        acceleroMagnetoSensor.registerNewSensorValueListener(this);
        magnetoSensor = new MagnetoSensor(getApplicationContext());
        magnetoSensor.registerNewSensorValueListener(this);

        // set current sensor
        currentSensor = acceleroMagnetoSensor;

        // setup transport
        try {
            sender = new DatagramSender();
            sender.start();
        } catch (SocketException e) {
            Snackbar.make(findViewById(R.id.constraintLayout), e.getMessage(), Snackbar.LENGTH_LONG).show();
            return;
        }

        setupUi();

        if (RUN_LATENCY_EVALUATION) {
            try {
                LatencyEvaluation eval = new LatencyEvaluation(getApplicationContext(), Short.parseShort(editText_remotePort.getText().toString()), sender);
                eval.start();
                Snackbar.make(findViewById(R.id.constraintLayout), "Latency Evaluation in progress!", Snackbar.LENGTH_INDEFINITE).show();
            } catch (SocketException e) {
                Snackbar.make(findViewById(R.id.constraintLayout), "Starting latency evaluation failed!", Snackbar.LENGTH_LONG).show();
                return;
            }
        }

        // statistics update loop
        (new Thread() {
            @Override
            public void run() {
                try {
                    while (!this.isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateStatistics();
                            }
                        });
                    }
                } catch (InterruptedException ignored) {}
            }
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!RUN_LATENCY_EVALUATION) currentSensor.enable(samplingInterval);
    }

    @Override
    protected void onPause() {
        super.onPause();
        currentSensor.disable();
        disableSending();
    }

    protected void setupUi() {
        setContentView(R.layout.activity_main);

        try {
            String version = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionName;
            setTitle("Orientation Sender v" + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        textView_sensorAzimuth = findViewById(R.id.textView_sensorAzimuth);
        textView_sensorPitch = findViewById(R.id.textView_sensorPitch);
        textView_sensorRoll = findViewById(R.id.textView_sensorRoll);
        textView_avgSamplingInterval = findViewById(R.id.textView_avgSamplingInterval);
        textView_numSamples = findViewById(R.id.textView_numSamples);
        button_toggleSending = findViewById(R.id.button_toggleSending);
        editText_remoteIpAddress = findViewById(R.id.editText_remoteIpAddress);
        editText_remotePort = findViewById(R.id.editText_remotePort);
        editText_customSamplingInterval = findViewById(R.id.editText_customSamplingInterval);
        editText_customSamplingInterval.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                samplingInterval = Integer.parseInt(editable.toString());
                currentSensor.disable();
                Snackbar.make(findViewById(R.id.constraintLayout), "Set sampling interval to " + samplingInterval + " microseconds!", Snackbar.LENGTH_LONG).show();
                currentSensor.enable(samplingInterval);
            }
        });
        spinner_samplingInterval = findViewById(R.id.spinner_samplingInterval);

        // set up sampling interval spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, SAMPLE_INTERVAL_OPTIONS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_samplingInterval.setAdapter(adapter);
        spinner_samplingInterval.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                currentSensor.disable();

                switch (i) {
                    case 0: // Fastest
                        samplingInterval = SensorManager.SENSOR_DELAY_FASTEST;
                        break;
                    case 1: // Game
                        samplingInterval = SensorManager.SENSOR_DELAY_GAME;
                        break;
                    case 2: // UI
                        samplingInterval = SensorManager.SENSOR_DELAY_UI;
                        break;
                    case 3: // Normal
                        samplingInterval = SensorManager.SENSOR_DELAY_NORMAL;
                        break;
                    case 4: // custom
                        currentSensor.disable();
                        samplingInterval = Integer.parseInt(editText_customSamplingInterval.getText().toString());
                        currentSensor.enable(samplingInterval);
                        Snackbar.make(findViewById(R.id.constraintLayout), "Set sampling interval to " + samplingInterval + " microseconds!", Snackbar.LENGTH_LONG).show();
                        break;
                }

                currentSensor.enable(samplingInterval);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                spinner_samplingInterval.setSelection(SensorManager.SENSOR_DELAY_NORMAL);
                Snackbar.make(findViewById(R.id.constraintLayout), "One sampling interval needs to be set!", Snackbar.LENGTH_LONG).show();
            }
        });
        spinner_samplingInterval.setSelection(SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void enableSending() {
        if (sendingEnabled) return;

        new Thread(() -> {
            String addrStr = editText_remoteIpAddress.getText().toString();
            if (addrStr.length() == 0) {
                Snackbar.make(findViewById(R.id.constraintLayout), "No IP address or hostname provided!", Snackbar.LENGTH_LONG).show();
                return;
            }
            try {
                inetAddress = InetAddress.getByName(addrStr);
            } catch (UnknownHostException e) {
                Snackbar.make(findViewById(R.id.constraintLayout), "Invalid IP address or hostname provided!", Snackbar.LENGTH_LONG).show();
                return;
            }

            port = Short.parseShort(editText_remotePort.getText().toString());

            button_toggleSending.setText("Stop Sender");
            sendingEnabled = true;
            Snackbar.make(findViewById(R.id.constraintLayout), "Started sending!", Snackbar.LENGTH_LONG).show();
        }).start();
    }

    protected void disableSending() {
        if (!sendingEnabled) return;
        button_toggleSending.setText("Start Sender");
        sendingEnabled = false;
        Snackbar.make(findViewById(R.id.constraintLayout), "Stopped sending!", Snackbar.LENGTH_LONG).show();
    }

    protected void updateStatistics() {
        textView_sensorAzimuth.setText(Float.toString(lastOrientation[0]));
        textView_sensorPitch.setText(Float.toString(lastOrientation[1]));
        textView_sensorRoll.setText(Float.toString(lastOrientation[2]));

        double avgSamplingInterval = samplingIntervalSum / numSamples;
        avgSamplingInterval /= 1000000; // nsec to msec
        textView_avgSamplingInterval.setText(Double.toString(avgSamplingInterval) + " msec");
        textView_numSamples.setText(Long.toString(numSamples));
    }

    public void onClick_button_toggleSending(View view) {
        if (!sendingEnabled) enableSending();
        else disableSending();
    }

    public void onClick_radioButton_sensorType(View view) {
        if (!((RadioButton) view).isChecked()) return;

        currentSensor.disable();

        int id = view.getId();
        if (id == R.id.radioButton_sensorType_acceleroGyro)
            currentSensor = acceleroGyroSensor;
        else if (id == R.id.radioButton_sensorType_acceleroMagneto)
            currentSensor = acceleroMagnetoSensor;
        else
            currentSensor = acceleroMagnetoSensor;

        currentSensor.enable(samplingInterval);
    }

    @Override
    public void onNewSensorValue(float[] orientation) {
        String azimuth = Float.toString(orientation[0]);
        String pitch = Float.toString(orientation[1]);
        String roll = Float.toString(orientation[2]);

        if (sendingEnabled) {
            // send orientation over transport
            String str = azimuth + ';' + pitch + ';' + roll;
            byte[] data = str.getBytes(StandardCharsets.UTF_8);
            DatagramPacket packet = new DatagramPacket(data, data.length, inetAddress, port);
            sender.enqueue(packet);
        }

        // update statistics
        long currentTime = System.nanoTime();
        lastOrientation[0] = orientation[0];
        lastOrientation[1] = orientation[1];
        lastOrientation[2] = orientation[2];
        ++numSamples;
        if (lastSampleTime > 0)
            samplingIntervalSum += currentTime - lastSampleTime;
        lastSampleTime = currentTime;
    }
}