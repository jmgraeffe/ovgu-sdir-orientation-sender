package de.ovgu.inf.sdir.orientationsender.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;

// https://developer.android.com/guide/topics/sensors/sensors_position#java
// https://source.android.com/docs/core/interaction/sensors/sensor-types
public class MagnetoSensor extends BaseSensor {
    protected final float[] rotationMatrix = new float[9];

    public MagnetoSensor(Context context) {
        super(context);
        sensorType = Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR;
    }

    // Get readings from accelerometer and magnetometer. To simplify calculations,
    // consider storing these readings as unit vectors.
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != sensorType) return;
        SensorManager.getRotationMatrixFromVector(rotationMatrix, event.values);
        SensorManager.getOrientation(rotationMatrix, orientationAngles);
        super.onSensorChanged(event);
    }
}
