package de.ovgu.inf.sdir.orientationsender.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;

// https://developer.android.com/guide/topics/sensors/sensors_position#java
// https://source.android.com/docs/core/interaction/sensors/sensor-types
public class AcceleroMagnetoSensor extends BaseSensor {
    protected final float[] rotationMatrix = new float[9];

    protected final float[] accelerometerReading = new float[3];
    protected final float[] magnetometerReading = new float[3];

    public AcceleroMagnetoSensor(Context context) {
        super(context);
    }

    @Override
    public void enable(int samplingInterval) {
        if (enabled) return;

        Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (accelerometer != null) {
            sensorManager.registerListener(this, accelerometer, samplingInterval);
        }
        Sensor magneticField = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        if (magneticField != null) {
            sensorManager.registerListener(this, magneticField, samplingInterval);
        }

        enabled = true;
    }

    @Override
    public void disable() {
        if (!enabled) return;
        sensorManager.unregisterListener(this);
        enabled = false;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            System.arraycopy(event.values, 0, accelerometerReading,
                    0, accelerometerReading.length);
        } else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            System.arraycopy(event.values, 0, magnetometerReading,
                    0, magnetometerReading.length);
        }

        SensorManager.getRotationMatrix(rotationMatrix, null, accelerometerReading, magnetometerReading);
        SensorManager.getOrientation(rotationMatrix, orientationAngles);
        super.onSensorChanged(event);
    }
}
