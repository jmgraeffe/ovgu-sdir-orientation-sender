package de.ovgu.inf.sdir.orientationsender.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.ArrayList;

public abstract class BaseSensor implements ISensor, SensorEventListener {
    protected boolean enabled = false;
    protected int sensorType = -1;
    protected final float[] orientationAngles = new float[3];
    protected SensorManager sensorManager;
    protected ArrayList<INewSensorValueListener> newSensorValueListeners = new ArrayList<>();

    public BaseSensor(Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //TODO do something here if sensor accuracy changes
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        for (INewSensorValueListener listener : newSensorValueListeners) {
            listener.onNewSensorValue(orientationAngles);
        }
    }

    @Override
    public void enable(int samplingInterval) {
        if (enabled) return;

        if (sensorType >= 0) {
            Sensor sensor = sensorManager.getDefaultSensor(sensorType);
            if (sensor != null)
                sensorManager.registerListener(this, sensor, samplingInterval);
        }

        enabled = true;
    }

    @Override
    public void disable() {
        if (!enabled) return;

        sensorManager.unregisterListener(this);

        enabled = false;
    }

    @Override
    public void registerNewSensorValueListener(INewSensorValueListener listener) {
        newSensorValueListeners.add(listener);
    }
}
