package de.ovgu.inf.sdir.orientationsender;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.ArrayBlockingQueue;

public class DatagramSender extends Thread {
    protected static final String TAG = DatagramSender.class.getName();
    protected static final int QUEUE_SIZE = 10;
    protected ArrayBlockingQueue<DatagramPacket> queue = new ArrayBlockingQueue<>(10);
    protected DatagramSocket socket = new DatagramSocket();

    public DatagramSender() throws SocketException {
        super();
        socket = new DatagramSocket();
    }

    @Override
    public void run() {
        while (true) {
            try {
                DatagramPacket packet = queue.take();
                socket.send(packet);
            } catch (InterruptedException e) {
                Log.d(TAG, "interrupted");
            } catch (IOException e) {
                Log.d(TAG, e.getMessage());
            }
        }
    }

    public void enqueue(DatagramPacket packet) {
        try {
            queue.add(packet);
        } catch (IllegalStateException e) {
            Log.d(TAG, "Queue is full. Think about increasing queue size or decreasing frequency!");
        }
    }
}
