package de.ovgu.inf.sdir.orientationsender;

import android.content.Context;
import android.hardware.SensorManager;
import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;

import de.ovgu.inf.sdir.orientationsender.sensors.AcceleroMagnetoSensor;

public class LatencyEvaluation extends Thread {
    protected static final String TAG = LatencyEvaluation.class.getName();
    protected DatagramSocket socket;
    protected DatagramSender sender;

    protected final float[] orientationAngles = new float[3];
    protected final float[] rotationMatrix = new float[9];
    protected final float[] accelerometerReading = new float[3];
    protected final float[] magnetometerReading = new float[3];

    public LatencyEvaluation(Context context, int port, DatagramSender sender) throws SocketException {
        socket = new DatagramSocket(port);
        this.sender = sender;
    }

    @Override
    public void run() {
        byte[] receivedData = new byte[1024];
        DatagramPacket receivedPacket = new DatagramPacket(receivedData, receivedData.length);
        while (true) {
            try {
                socket.receive(receivedPacket);
                SensorManager.getRotationMatrix(rotationMatrix, null, accelerometerReading, magnetometerReading);
                SensorManager.getOrientation(rotationMatrix, orientationAngles);
                String azimuth = Float.toString(1.9114987f);
                String pitch = Float.toString(-0.3312756f);
                String roll = Float.toString(-0.49620938f);

                // send orientation over transport
                String str = azimuth + ';' + pitch + ';' + roll;
                byte[] sentData = str.getBytes(StandardCharsets.UTF_8);
                DatagramPacket packet = new DatagramPacket(sentData, sentData.length, receivedPacket.getAddress(), receivedPacket.getPort());
                sender.enqueue(packet);
            } catch (IOException e) {
                Log.d(TAG, e.getMessage());
            }
        }
    }
}
