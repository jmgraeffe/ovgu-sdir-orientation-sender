package de.ovgu.inf.sdir.orientationsender.sensors;

public interface ISensor {
    void enable(int samplingInterval);
    void disable();
    void registerNewSensorValueListener(INewSensorValueListener listener);
}
