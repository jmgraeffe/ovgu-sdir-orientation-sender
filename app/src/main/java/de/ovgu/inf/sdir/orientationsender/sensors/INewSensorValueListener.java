package de.ovgu.inf.sdir.orientationsender.sensors;

public interface INewSensorValueListener {
    void onNewSensorValue(float orientation[]);
}
