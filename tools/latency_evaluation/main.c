#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define BUF_LEN (64)
#define NUM_ITERATIONS (1000)

static void ts_diff(struct timespec* start, struct timespec* end, struct timespec* diff)
{
	if ((end->tv_nsec - start->tv_nsec) < 0) {
		diff->tv_sec = end->tv_sec - start->tv_sec - 1;
		diff->tv_nsec = 1000000000 + end->tv_nsec - start->tv_nsec;
	} else {
		diff->tv_sec = end->tv_sec - start->tv_sec;
		diff->tv_nsec = end->tv_nsec - start->tv_nsec;
	}
}

int main(int argc, char const *argv[])
{
	int sockfd;
	struct sockaddr_in in_addr, out_addr;
	uint8_t buf[BUF_LEN] = { 0 };

	if (argc != 3) {
		printf("Invalid arguments! Usage: ./latency_evaluation <ip> <port>\n");
		exit(EXIT_FAILURE);
	}

	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		printf("Socket creation failed (%s)!\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	uint16_t port = htons(atoi(argv[2]));

	// address to receive from
	memset(&in_addr, 0, sizeof(in_addr));
	in_addr.sin_family = AF_INET; // IPv4
	in_addr.sin_addr.s_addr = INADDR_ANY;
	in_addr.sin_port = port;

	// address to send to
	memset(&out_addr, 0, sizeof(out_addr));
	out_addr.sin_family = AF_INET; // IPv4
	out_addr.sin_addr.s_addr = inet_addr(argv[1]);
	out_addr.sin_port = port;

	if (bind(sockfd, (const struct sockaddr*) &in_addr, sizeof(in_addr)) < 0) {
		printf("Binding to socket failed (%s)!\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	for (int i = 0; i < NUM_ITERATIONS; ++i) {
		struct timespec start, end, diff;
		clock_gettime(CLOCK_MONOTONIC, &start);

		// send "request"
		ssize_t len = sendto(sockfd, buf, 0, 0, (struct sockaddr*) &out_addr, sizeof(out_addr));
		if (len < 0) {
			printf("Error while sending data (%s)!\n", strerror(errno));
			continue;
		}

		//usleep(1000);

		// receive "response"
		len = recvfrom(sockfd, buf, BUF_LEN, MSG_WAITALL, NULL, NULL);
		if (len < 0) {
			printf("Error while receiving data (%s)!\n", strerror(errno));
			continue;
		} else if (len > BUF_LEN - 1) {
			printf("Packet too large!\n");
			continue;
		}

		buf[len] = '\0';
		//printf("%s\n", buf);

		double azimuth, pitch, roll;
		int res = sscanf((const char*) buf, "%lf;%lf;%lf", &azimuth, &pitch, &roll);
		if (res != 3) {
			printf("Invalid content!\n");
			continue;
		}
		//printf("%lf;%lf;%lf\n", azimuth, pitch, roll);

		clock_gettime(CLOCK_MONOTONIC, &end);
		ts_diff(&start, &end, &diff);
		printf("%lu\n", diff.tv_sec * 1000000000 + diff.tv_nsec);
		usleep(5000);
	}

	return 0;
}